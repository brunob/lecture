# Programming Techniques for Scientific Simulations, ETH Zürich

set title 'Simpson rule: convergence for {/Symbol I}=∫sin(x)dx over [0,{/Symbol p}]'
set xlabel 'Resultion {/Symbol N}'
set ylabel "Integral\nvalue {/Symbol I}" offset 5,0.5 rotate by 0
set lmargin 14

set yrange [1.999:]
#~ set log x
set grid
set key box

set terminal png
set output 'data/convergence.png'

plot 'data/results.txt' title 'libintegrate.a results' with linespoints,\
     2 title 'analytic solution'
