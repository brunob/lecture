# Programming Techniques for Scientific Simulations I

Thursday 13:45 - 17:30, [HCI J3](http://www.rauminfo.ethz.ch/Rauminfo/grundrissplan.gif?region=Z&areal=H&gebaeude=HCI&geschoss=J&raumNr=3)

This lecture provides an overview of programming techniques for scientific
simulations.
The focus is on advanced C++ programming techniques and scientific software
libraries.
Based on an overview over the hardware components of PCs and supercomputers,
optimization methods for scientific simulation codes are explained.

For questions or remarks we have a mailing list where you can reach us:
<pt1_hs19@sympa.ethz.ch >

## Lecture slides, exercises and solutions

Lecture slides, exercise sheets and solutions will be provided as part of this
git repository.

## Submission

If you want to receive feedback on your exercises, please push your solutions
to your own git repository before **Monday night** of the week after we hand
out the exercise sheet.
Then send a notification / request for correction email (possibly with
questions) to the mailing list.
Advanced users can utilise GitLab issues (make sure to tag all the assistants,
but not the professor, with their @name in the issue description).

Your exercise will then be corrected before the next exercise session.
Make sure to give *maintainer* access to the following people:
@karoger, @cmarcati, @engelerp, @gbalduzz and @msudwoj.

## Course confirmation (Testat)

For students needing the confirmation (Testat) for this course, we require
that 70% of the exercises have been solved reasonably (sinnvoll).
The submission deadline is every Wednesday midnight (Zurich time!).

Please announce that you want the confirmation (Testat) for this course
explicitly at the beginning of the semester. Contact us either in person or
through the mailing list.

## Exam information

* The exam will have two parts: A written theoretical part, and a programming
  part that you will solve on the exam computers

* The exam computers will run Fedora Linux, similar to those that you find in
  the computer rooms in the ETH main building.
  The system language of the computers is English.
  A list of the installed software can be found [here](https://www.ethz.ch/services/en/it-services/catalogue/managed-client/computer-rooms.html).

* By default, the keyboards will have the Swiss layout.
  There will be a poll for those who want to get a US keyboard instead.

* Provided on the computers are:
    * The full lecture repository
    * The C++ standard (draft version)
    * An offline version of http://www.cppreference.com
    * An offline version of [the Python documentation](https://docs.python.org/3.6/index.html)
    * As needed, offline versions of the documentation for Python libraries

* This is an open-book exam, which means that you can bring any written
  material (books, notes, printed code, ...).
  However, you may **not** use any digital devices (other than the exam
  computer) during the exam.

* Don't forget to bring your student card (Legi).

