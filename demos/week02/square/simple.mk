.PHONY: all
all: square.exe

square.o: square.cpp square.hpp
	c++ -c square.cpp

main.o: main.cpp square.hpp
	c++ -c main.cpp

square.exe: main.o square.o
	c++ -o square.exe main.o square.o

